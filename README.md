
## Weather Extension

A tiny chrome extension to keep a user with the current weather conditions.

## Questionnaire

### User Interface

The implemented extension has an icon and a badge representing current
weather conditions and temperature. On icon click it shows a popup,
which contains, besides weather icon and temperature, determined location,
weather short description and time of the actualization.

### Data source

The extension has to query a third-party weather provider called OpenWeather
in order to get actual conditions. After registration procedure we got a key
to use with our requests.

### Browser choice

I've chosen Chrome as a starting platform as it has clear documentation
supported by Google. In future it should be possible the extension to port
to other platforms.

### Data flow

There is a background part, that queries OpenWeather API periodically
(configurable), stores result in a state object and updates the icon. In case
of an error, the error will be logged, retried in some time (configurable),
but the state and actualization time will remain unchanged.

Popup part, on each launch, will send a message to background asking for the
weather data, background responses with the state object and it will be
rendered in the popup.

### Technologies

I've tried to choose modern and well maintained frameworks:

- `gulp` - for building things
- `mustache` - simple template engine to render popup
- `eslint` plus airbnb convention for the code quality
- `browserify` - to bundle javascript code together
- `mocha`, `chai`, `sinon`, `jsdom` - for unit testing (along with `babel` to be able to test ES6 code)
- `openweathermap` - tiny OpenWeather wrapper not to write fetches on our own

### Testing

I've added unit tests for some important parts of the application.

### User Base growth

Since OpenWeather API provides only limited free plan (~600requests/10min),
we have to consider in future either to use some paid plan or to implement
some caching proxy, with which our extension will communicate instead of
OpenWeather directly.

## Build steps

- make sure you have actual `nodejs` and `npm` versions
- run `npm i`
- run `./node_modules/.bin/gulp`
- point your chrome browser to the `./build` folder


## TODO things

- add minify
- browserify watch mode and source maps
- extend error handling
- build crx file.
- add test coverage
