const gulp = require('gulp');
const clean = require('gulp-clean');
const eslint = require('gulp-eslint');
const mocha = require('gulp-mocha');
const browserify = require('browserify');
const source = require('vinyl-source-stream');

require('babel-core/register');

gulp.task('clean', () => {
  gulp.src('build/*', { read: false }).pipe(clean());
});

gulp.task('copy', () => {
  gulp.src('icons/**')
    .pipe(gulp.dest('build/icons'));

  gulp.src('chrome/manifest.json')
    .pipe(gulp.dest('build'));

  gulp.src('src/popup/*.html')
    .pipe(gulp.dest('build'));

  gulp.src('src/popup/*.css')
    .pipe(gulp.dest('build'));
});

gulp.task('eslint', () => {
  gulp.src('src/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('tests', () => {
  gulp.src('tests/*.js', { read: false })
    .pipe(mocha({ reporter: 'nyan' }));
});

gulp.task('scripts', ['eslint', 'tests'], () => {
  browserify('src/popup/popup.js')
    .bundle()
    .pipe(source('popup.js'))
    .pipe(gulp.dest('build'));

  browserify('src/background/background.js')
    .bundle()
    .pipe(source('background.js'))
    .pipe(gulp.dest('build'));
});

gulp.task('crx', ['scripts', 'copy'], () => {
  //TODO: implement
});

gulp.task('default', ['clean'], () => {
  gulp.start('crx');
});
