const config = require('./conf.js');
const updateIcon = require('./updateIcon.js');
const weather = require('./weather.js');

const weatherData = {};

function fetchWeather() {
  navigator.geolocation.getCurrentPosition((position) => {
    const lat = position.coords.latitude;
    const lon = position.coords.longitude;
    weather.getCurrent(lat, lon, (error, data) => {
      if (error) {
        // eslint-disable-next-line no-console
        console.error(error);

        chrome.alarms.create(
          'fetchWeatherRetry',
          { delayInMinutes: config.retryDelay }
        );
      } else {
        weatherData.data = data;
        weatherData.actualizedAt = Date.now();

        updateIcon(data.iconName, data.temperature);
      }
    });
  });
}

fetchWeather();

chrome.alarms.create(
  'fetchWeather',
  { periodInMinutes: config.fetchPeriod }
);

chrome.alarms.onAlarm.addListener((alarm) => {
  if (alarm.name.indexOf('fetchWeather') === 0) {
    fetchWeather();
  }
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.fetchWeather) {
    sendResponse(weatherData);
  }
});

chrome.browserAction.setBadgeBackgroundColor({
  color: config.badgeRGBA,
});
