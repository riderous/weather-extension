const utils = require('../utils.js');

module.exports = (iconName, temperature) => {
  chrome.browserAction.setIcon({
    path: utils.getIconPath(iconName),
  });

  chrome.browserAction.setBadgeText({
    text: utils.getTemperatureString(temperature),
  });
};
