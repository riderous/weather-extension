const openweather = require('openweathermap');
const config = require('./conf.js');

module.exports.getCurrent = (latitude, longitude, callback) => {
  openweather.now({
    APPID: config.openweather.key,
    lat: latitude,
    lon: longitude,
    units: 'metric',
    lang: 'en',
  }, (error, result) => {
    if (error) {
      callback(error);
    } else {
      callback(null, {
        temperature: result.main.temp,
        iconName: result.weather[0].icon,
        location: result.name,
        description: result.weather[0].description,
      });
    }
  });
};
