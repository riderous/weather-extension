const renderTemplate = require('./renderTemplate.js');

chrome.runtime.sendMessage({ fetchWeather: true }, (response) => {
  if (response.data) {
    renderTemplate(response.data, response.actualizedAt);
  }
});
