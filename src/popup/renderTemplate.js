const Mustache = require('mustache');
const utils = require('../utils.js');

module.exports = (weatherData, actualizedAt) => {
  const date = new Date(actualizedAt);
  const hours = utils.addTimePadding(date.getHours());
  const minutes = utils.addTimePadding(date.getMinutes());

  document.getElementById('target').innerHTML = Mustache.to_html(
    document.getElementById('template').innerHTML, {
      location: weatherData.location,
      description: weatherData.description,
      icon: utils.getIconPath(weatherData.iconName),
      temperature: utils.getTemperatureString(weatherData.temperature),
      actualizedAt: `${hours}:${minutes}`,
    });
};
