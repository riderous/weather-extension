module.exports.getTemperatureString = (temperature) => {
  let result = `${Math.ceil(temperature)}`;
  if (temperature > 0) {
    result = `+${result}`;
  }
  return result;
};

module.exports.getIconPath = (iconName) => {
  return `icons/${iconName}.png`;
};

module.exports.addTimePadding = (value) => {
  return value < 10 ? `0${value}` : value;
};
