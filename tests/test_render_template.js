const assert = require('chai').assert;
const sinon = require('sinon');
const jsdom = require('jsdom').jsdom;

const renderTemplate = require('../src/popup/renderTemplate.js');

describe('renderTemplate', () => {
  beforeEach(() => {
    global.document = jsdom('<div id="target"></div><div id="template"></div>');
  });

  it('should render location in the template placeholder', () => {
    const testLocation = 'Lviv';
    document.getElementById('template').innerHTML = '{{ location }}';
    renderTemplate({ location: testLocation }, 0);
    assert.equal(document.getElementById('target').innerHTML, testLocation);
  });

  it('should render description in the template placeholder', () => {
    const testDescription = 'some description';
    document.getElementById('template').innerHTML = '{{ description }}';
    renderTemplate({ description: testDescription }, 0);
    assert.equal(
      document.getElementById('target').innerHTML,
      testDescription
    );
  });

  it('should render icon path in the template placeholder', () => {
    document.getElementById('template').innerHTML = '{{ icon }}';
    renderTemplate({ iconName: 'iconName' }, 0);
    assert.equal(
      document.getElementById('target').innerHTML,
      'icons/iconName.png'
    );
  });

  it('should render temperature in the template placeholder', () => {
    document.getElementById('template').innerHTML = '{{ temperature }}';
    renderTemplate({ temperature: 10 }, 0);
    assert.equal(document.getElementById('target').innerHTML, '+10');
  });

  it('should render time in the template placeholder (epoch, CET)', () => {
    document.getElementById('template').innerHTML = '{{ actualizedAt }}';
    renderTemplate({}, 0);
    assert.equal(document.getElementById('target').innerHTML, '01:00');
  });
});
