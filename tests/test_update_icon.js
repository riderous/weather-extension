const assert = require('chai').assert;

const updateIcon = require('../src/background/updateIcon.js');

describe('updateIcon', () => {
  before(() => {
     global.chrome = require('sinon-chrome');
  });

  it('should set badge and icon', () => {
    updateIcon('iconName', 10);
    assert(
      chrome.browserAction.setBadgeText
        .withArgs({ text: '+10' })
        .calledOnce);
    assert(
      chrome.browserAction.setIcon
        .withArgs({ path: 'icons/iconName.png' })
        .calledOnce);
  });
});
