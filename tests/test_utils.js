const assert = require('chai').assert;
const utils = require('../src/utils.js');

describe('utils', () => {
  describe('addTimePadding', () => {
    it('should return a string w/ left zero padding for values < 10', () => {
      assert.equal(utils.addTimePadding(0), '00');
      assert.equal(utils.addTimePadding(1), '01');
      assert.equal(utils.addTimePadding(9), '09');
    });

    it('should return a string w/o left zero padding for values > 10', () => {
      assert.equal(utils.addTimePadding(10), '10');
      assert.equal(utils.addTimePadding(24), '24');
    });
  });

  describe('getIconPath', () => {
    it('should return full icon path w/ png extension', () => {
      assert.equal(utils.getIconPath('someicon'), 'icons/someicon.png');
    });
  });

  describe('getTemperatureString', () => {
    it('should return a string w/ plus sign for positives values', () => {
      assert.equal(utils.getTemperatureString(1), '+1');
      assert.equal(utils.getTemperatureString(20), '+20');
    });

    it('should return a string w/o plus sign for negative values', () => {
      assert.equal(utils.getTemperatureString(-1), '-1');
      assert.equal(utils.getTemperatureString(-20), '-20');
    });

    it('should return a string w/o plus sign for 0', () => {
      assert.equal(utils.getTemperatureString(0), '0');
    });
  });
});
