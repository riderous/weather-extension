const assert = require('chai').assert;
const sinon = require('sinon');
const openweather = require('openweathermap');

const config = require('../src/background/conf.js');
const weather = require('../src/background/weather.js');

describe('weather', () => {
  describe('getCurrent', () => {
    afterEach(() => {
      openweather.now.restore();
    });

    it('should catch an error from openweather', (done) => {
      sinon.stub(openweather, 'now', (cfg, cb) => {
        cb('someError');
      });

      weather.getCurrent(1, 2, (error) => {
        assert.equal(error, 'someError');
        done();
      });
    });

    it('should call openweather.now with expected arguments', (done) => {
      const nowStub = sinon.stub(openweather, 'now', (cfg, cb) => {
        cb('someError');
      });
      weather.getCurrent(1, 2, (error) => {
        done();
      });
      assert(nowStub.calledWith({
        APPID: config.openweather.key,
        lat: 1,
        lon: 2,
        units: 'metric',
        lang: 'en',
      }));
    });

    it('should retun data in the expected format', (done) => {
      sinon.stub(openweather, 'now', (cfg, cb) => {
        cb(null, {
          main: { temp: 10 },
          weather: [{ icon: 'someIcon', description: 'someDescription' }],
          name: 'someName',
        });
      });
      weather.getCurrent(1, 2, (error, data) => {
        assert.equal(data.temperature, 10);
        assert.equal(data.iconName, 'someIcon');
        assert.equal(data.location, 'someName');
        assert.equal(data.description, 'someDescription');
        done();
      });
    });
  });
});
